I use this to document some interesting programming challenges I have come across as well as some basic programming 
concepts like dynamic programming, sorting, searching and some basic to advanced data structures like trees, priority queues etc.

The structure of the solution i.e. *.java file is
 The problem
 	The problem statment is explained 
 The algorithm
 	A brief explanation of the method/algorithm used
 Pseudocode
 	The pseudocode is presented
 Example
 	Contains the explanation of the pseudocode presented above
 
 Keep Calm and Code on!