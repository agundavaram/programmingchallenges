package com.algos.sorting;

import com.ds.MaxPQ;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 * 
 * Heapsort uses a priority queue implementation using a heap to sort an input sequence. 
 * All the elements of the input sequence are sequentially inserted into the queue and
 * then rapidly removed and printed to give a a decreasing sorted sequence.  
 *  
 *
 */
public class HeapSort {

	public static void main(String[] args) {

		int []input = {10,1,101,30,45,67,89,34,21,11,10,3};
		sort(input);
	}

	public static void sort(int []input){
		MaxPQ pq = new MaxPQ(input.length);
		for(int i : input){
			pq.insert(i);
		}

		System.out.print("Given Sequence:  ");
		for(int i : input){
			System.out.print(i+" ");
		}
		System.out.print("\nSorted Sequence: ");
		while(!pq.isEmpty()){
			System.out.print(pq.delMax()+" ");
		}
	}
}
