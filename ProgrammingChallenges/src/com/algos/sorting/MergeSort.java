package com.algos.sorting;

import java.security.InvalidParameterException;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 *
 *  Problem:
 *   Sort a given set of integers, using MergeSort
 *  
 *  Algorithm:
 *   Mergesort is implemented. It is an example of divide and conquor method. In this method the input is broken into smaller parts, solved and re-combined to get the output.
 *   Thus the input array is broken into halves recursively and sorted. The sorted halves are then merged. 
 *   
 *  Pseudocode and Example
 *    The following is the implementation of mergesort as given by Robert Segdewick and Kevin Wayne in their book Algorithms (http://algs4.cs.princeton.edu/22mergesort/)
 */
public class MergeSort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int []a = {2,4,3,6,1,9,8};
		int []aux = new int[a.length]; // An auxilary array is created and passed to the sort method
		sort(a, aux, 0, a.length-1);
		
		System.out.print("Sorted sequence is: ");
		for(int i = 0; i < a.length; i++){
			System.out.print(a[i]+" ");
		}
	}

	public static void merge(int[] a, int[] aux, int lo, int mid, int hi){
		
		// We are making a copy of the input array to an auxiliary array
		for(int k = lo; k <=hi; k++){
			aux[k] = a[k];
		}
		
		// The input array given is sorted between low to mid and mid+1 to high, thus we use two index i and j one pointing to the start of sorted lowerhalf and second to sorted upper half
		int i = lo;
		int j = mid+1;
		
		/* For every element k i.e. for the size of the array, we loop through it and check four cases 
		 * If the value at i is greater than value at j, then copy the value at j to k and increment j
		 * else copy the value of i to k and increment the value of i
		 * The other cases we need to handle are when one of the array is exhausted, 
		 *  if the i is looped till the mid point, then we have to keep copying j to k and incrementing j
		 *  similarly if j reached high, i.e. the last element in then we have to keep copying i to k and incrementing i.  */
		
        for (int k = lo; k <= hi; k++) {
            if (i > mid)   
            	a[k] = aux[j++];   
            else if (j > hi)        
            	a[k] = aux[i++];
            else if (aux[i] < aux[j]) 
            	a[k] = aux[i++];
            else               
            	a[k] = aux[j++];
        }
	}
	
	public static void sort(int a[], int aux[], int lo, int hi){
		if(a == null)
			throw new InvalidParameterException("Input sequence null is not valid");
		if (hi <= lo) 
			return;
        int mid = lo + (hi - lo) / 2;
        sort(a, aux, lo, mid);
        sort(a, aux, mid + 1, hi);
        merge(a, aux, lo, mid, hi);
	}
}
