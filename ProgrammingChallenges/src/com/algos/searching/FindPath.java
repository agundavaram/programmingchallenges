package com.algos.searching;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import com.ds.Graph;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 * This class finds the path between a source S and a vertex v using BFS. The graph is pre-processed first in the method processGraph, using the graph g and string s as source. 
 * This method then populates dist, where dist[v] gives the distance between v and s and prev can be used to backtrack the path from v to s. This implementation is based on Robert Sedgewick
 *  and Kevin Wayne's implementation in their book Algorithms. For more details, visit http://introcs.cs.princeton.edu/java/home/
 */
public class FindPath {
	
	private Map<String, Integer> dist = new HashMap<String, Integer>();
	private Map<String, String>  prev = new HashMap<String, String>();

	public void processGraph(Graph g, String s){
		Queue<String> queue = new LinkedList<String>();
		queue.add(s);
		dist.put(s, 0);

		while(!queue.isEmpty()){
			String vertex = queue.remove();
			for(String edge: g.getAdjacent(vertex)){
				if(!dist.containsKey(edge)){
					queue.add(edge);
					dist.put(edge, 1+dist.get(vertex));
					prev.put(edge, vertex);
				}
			}
		}
	}
	
	public  boolean isConnected(String v){
		if(!dist.containsKey(v))
			return false;
		return true;
	}
	
	public int getDistance(String v){
		if(!isConnected(v))
			return Integer.MAX_VALUE;
		return dist.get(v);
	}
	
	public  void printPath(String v){
		Stack<String> stack = new Stack<String>();
		while( v != null && dist.containsKey(v)){
			stack.push(v);
			v = prev.get(v);
		}
		while(!stack.isEmpty()){
			System.out.print(stack.pop()+" ");
		}
	}
    

}
