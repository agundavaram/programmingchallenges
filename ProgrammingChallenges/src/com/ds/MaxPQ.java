package com.ds;

import java.util.EmptyStackException;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 * This implementation is based on the MaxPQ implementation provided in Robert Sedgewick and Kevin Wayne's Algorithms
 */
public class MaxPQ {

	/**
	 * This is an implementation of priority queue using an array and the concepts of sink and swim to heapify. Operations/methods available are explained in detail below
	 * Insert
	 *   When inserting a new element, we insert it at the end of the array or bottom of the heap and swim the element to it's right position
	 *   Swim
	 *     This method takes an index k, and compares the value at k to it's parents i.e. k/2
	 *     If the value of the parent is less than the child's, they are exchanged and this process is looped till the queue is heapified. 
	 *     Thus the element "swims" to it's correct place in the queue 
	 * Delete
	 *    When deleting an element, since this is a maximum priority queue, the max value is stored at the top. To remove this, it is exchanged with the bottommost element of the queue/heap
	 *    and then removed. The bottom-most element is now the root and a sink operation is performed on it.
	 *    Sink
	 *     This method takes an index k, and compares its value with its highest child (child with the highest value) i.e. either 2k or 2k+1, let this be represented by j. 
	 *     If it is less than one of it's children, it is exchanged with its children and this process loops till either the end of the queue is reached or the parent is greater than both its children,
	 *     thus heapifying the queue
	 * IsEmpty
	 *    This returns true, if the heap is empty
	 * getMax
	 * 	  This returns the maximum value, in this case the root of the heap, without removing it from the heap
	 *   
	 * This implemention uses a array thus the size of the queue/heap is static. However this can be overcome by resizing during insertion as well as deletion
	 *  
	 */
	// This represents the Priority Queue and the number of elements contained by the priority queue
	int pq[];
	int numOfElem;

	public MaxPQ(int size){
		this.pq = new int[size+1];  // The size for the queue is given sixe + 1, as we ignore the first element, i.e. index 0 
		this.numOfElem = 0;
	}
	
	
	public void insert (int value){
		this.pq[++numOfElem] = value;
		swim(numOfElem);
	}

	public int delMax(){
		int max = this.pq[1];
		exch(1, numOfElem--);
		sink(1);
		return max;
	}

	public boolean isEmpty(){
		return numOfElem == 0;
	}
	
	public int getMax(){
		if(!isEmpty())
			return this.pq[1];
		throw new EmptyStackException();
	}
	private void sink(int k){
		while(2*k <= numOfElem ){
			int j = 2*k;
			if(j < numOfElem && less(j, j+1))
				j++;
			if(!less(k,j))
				break;
			exch(k,j);
			k = j;
		}
	}

	private void swim(int k){
		while(k > 1 && less(k/2, k)){
			exch(k,k/2);
			k = k/2;
		}
	}

	private void exch(int i, int j){
		int tmp = this.pq[i];
		this.pq[i] = this.pq[j];
		this.pq[j] = tmp;
	}

	private boolean less(int i, int j){
		if (this.pq[i] <= this.pq[j])
			return true;
		else 
			return false;
	}

}
