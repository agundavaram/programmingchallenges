package com.ds;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 * The following in an implementation of Graph using adjacency list represented by vertices. 
 */
public class Graph {

	private Map<String, Set<String>> vertices;
	private int E; 
	
	public Graph(){
		vertices = new HashMap<String, Set<String>>();
		E = 0;
	}
	
	public void addEdge(String w, String v) throws InvalidParameterException{
		if(w == null || v == null)
			throw new InvalidParameterException();
		w = w.toLowerCase();
		v = v.toLowerCase();
		if(hasEdge(w, v))
			return;
		if(!hasVertex(v) || !hasVertex(w))
			throw new InvalidParameterException("The input vertices are not part of the graph");
		vertices.get(v).add(w);
		vertices.get(w).add(v);
	}
	
	public void addVertex(String v){
	   if(hasVertex(v))
		   return;
	   vertices.put(v.toLowerCase(), new HashSet<String>());
	}
	
	public Set<String> getAdjacent(String v) throws InvalidParameterException{
		if(!hasVertex(v))
			throw new InvalidParameterException("Given vertex does not exist");
		return vertices.get(v);
	}
	
	public int getNumberOfVertices(){
		return vertices.size();
	}
	
	public int getNumberOfEdges(){
		return E;
	}
	
	public void printGraph(){
		if(vertices.isEmpty())
			System.out.println("Graph is empty");
		for(Map.Entry<String, Set<String>> vertex: vertices.entrySet()){
			System.out.print(vertex.getKey()+": ");
			for(String edge: vertices.get(vertex.getKey())){
				System.out.print(edge+",");
			}
			System.out.println(" ");
		}
		
	}
	private boolean hasVertex(String v){
		return vertices.containsKey(v.toLowerCase());
	}
	
	private boolean hasEdge(String w, String v){
		if(!hasVertex(w) || !hasVertex(v))
		  return false;
		return vertices.get(w).contains(v.toLowerCase());
	}
}

