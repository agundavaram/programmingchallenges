package com.ds.problems;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import com.algos.searching.FindPath;
import com.ds.Graph;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 * This class uses FindPath(which is implemented using breadth first search) to find the shortest path between a source ("jami") and various vertices ("yj", "laila").
 *  A simple graph is created using createGraph method.
 */
public class ShortestPathBetweenVertices {

	public static void main(String []args){
		Graph graph = new Graph();
		FindPath findPath = new FindPath();
		String source = "jami";
		createGraph(graph);
        String vertex = "laila";
		findPath.processGraph(graph, source);
		System.out.println("Shortest path between jami and "+vertex);
		findPath.printPath(vertex);
		System.out.println("\nShortest path between jami and "+vertex);
		findPath.printPath(vertex);
	}


	public static void createGraph(Graph graph){
		graph.addVertex("muggy");
		graph.addVertex("jami");
		graph.addVertex("Yj");
		graph.addVertex("divo");
		graph.addVertex("Laila");
		graph.addEdge("muggy", "laila");
		graph.addEdge("muggy", "jami");
		graph.addEdge("laila", "YJ");
		graph.addEdge("divo", "YJ");
		graph.addEdge("divo", "jami");
	}
}
