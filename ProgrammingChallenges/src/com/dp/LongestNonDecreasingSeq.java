package com.dp;

import java.security.InvalidParameterException;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 *
 */
public class LongestNonDecreasingSeq {

	/**
	 * Problem
	 *   Given a sequence of N numbers - A[1] , A[2] , ..., A[N] . 
	 *   Find the length of the longest non-decreasing sequence.
	 *   Print the longest non-decreasing sequence.
	 *
	 * Algorithm
	 *   This can be solved using a dynamic programming approach as it has both optimal sub-strucutre 
	 *   and overlapping sub-problems
	 *   
	 *   The longest sequence is computed using the following logic.
	 *   Let i represent the start of the current longest sequence 
	 *   and m represent it's length. Using Memoization, the longest sequence is calculated for every
	 *   index till the last state N, i.e. end of array is reached. The logic is to compare at each point i [0, N-1] to j [0, i-1] is the value at i is greater than j
	 *   and if the value of subsequence ending at i is less than the value of subsequence ending at j, plus 1. The second check is placed to avoid being overwritten by lesser values
	 *   as at each i, j loops from 0 to i-1. Thus we increment the value of subsequence ending at i, this array is represented by m.
	 *   Thus at each point, we look up values from m. 
	 *   
	 *   Pseudocode:
	 *   int in[] // holds the input sequence
	 *   int m[] // is used for memoization i.e. m[i] gives the length of the non-decreasing subsequence ending at i
	 *   int seq[] // holds the indices for the actual sequence in reverse order. i.e. seq[i] represents the index of next element in the non-decreasing sequence
	 *   int maxLength // used to keep track of the max length sub sequence encountered so far
	 *   int lastIndex // this in combination with seq, will give the non-decreasing sequence
	 *   
	 *   for i from 0 to N-1
	 *     for j from i-1 to 0
	 *        if in[i] > in[j] && m[i] < m[j] + 1
	 *             m[i] = m[j] + 1
	 *             seq[i] = j  // we link the sequence
	 *             
	 *        if maxLength < m[i]  // this if keeps track of the length of the longest sequence so far along with its last index i.e. i
	 *           maxLength = m[i]
	 *           lastIndex = i  
	 *     
	 *   output 
	 *   length is maxLength
	 *   sequence can be printed by backtracking seq using lastIndex
	 *  
	 *   Consider the following inputs
	 *   Seq: 3 4 2 5 7 8 6
	 *   
	 *   
	 * @param args
	 */
	public static void main(String[] args) {
		int []seq = {3,4,2,5,7,8,6};
		computeLongestSequence(seq);

		/*seq = null;
		computeLongestSequence(seq);*/
	}

	public static void computeLongestSequence(int[] inputSeq) throws InvalidParameterException{
		if(inputSeq == null)
			throw new InvalidParameterException("Input sequence null is not valid");
		int maxLength = 1;
		int lastIndex = 0;
		int m[] = new int[inputSeq.length + 1];
		int outputSeq[] = new int[inputSeq.length + 1];

		for(int i = 0; i < m.length; i++)
			m[i] = 1;   // Since each element will be a sub sequence
		for(int i = 0; i < outputSeq.length; i++)
			outputSeq[0] = -1; 
		
		for (int i = 1; i < inputSeq.length; i++){
			m[i] = 1;
			outputSeq[i] = -1; // instantiate to an invalid index

			for (int j = i - 1; j >= 0; j--){
				if (inputSeq[j]  < inputSeq[i] && m[j] + 1 > m[i]  ){
					m[i] = m[j] + 1;
					outputSeq[i] = j;
				}
			}
			if (m[i] > maxLength){
				lastIndex = i;
				maxLength = m[i];
			}
		}
		System.out.println("Length is "+maxLength);
		
		System.out.print("Sequence is "+inputSeq[lastIndex]);
		while(outputSeq[lastIndex] != -1){
			System.out.print(" " +inputSeq[outputSeq[lastIndex]]);
			lastIndex = outputSeq[lastIndex];
		}
	}
}


