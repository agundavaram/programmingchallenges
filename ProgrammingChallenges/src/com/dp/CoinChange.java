package com.dp;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CoinChange {

	/**
	 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
	 */

	/**
	 * 
	 * Problem
	 *  Given a list of N coins and their values (V1, V2, ... , VN), and the total sum S.
	 *  Find the minimum number of coins the sum of which is S (we can use as many coins of one type as we want), 
	 *  or report that it's not possible to select coins in such a way that they sum up to S 
	 *  
	 *  Algorithm
	 *    This can be solved using dynamic programming since it has both optimal substructure and overlapping subproblems approach in a bottom up manner.
	 *    At every point Si, we look at the minimum number of coins needed to calculate the previous S(i-1) and then add to it. We know this C[S(i-1)]to be the optimal
	 *    solution, thus C[Si] will be the optimal solution.
	 *    
	 *    For each value of i [0, S], we loop through the coin values j [0, N-1] such that if the value of the coin Vj is less or equal than the sum i.e. i, we look at
	 *    the coins that were needed for i - Vj. Let us assume this value to be m. Then the coins needed for i will be m + 1. 
	 *    
	 *        
	 *   Pseudocode
	 *   V[] // contains the value/denominations of the coins used V1 .... VN
	 *   C[] // C[i] contains the number of coins needed to provide a sum at i
	 *   C[0] = 0
	 *   Sum // the sum given
	 *   Map<Integer, Integer> coinCount
	 *   
	 *   for i from 1 to S
	 *     C[i] = infinity
	 *   for i from 1 to S
	 *   	 j from 0 to N - 1
	 *       if(V[j] <= i and C[i- V[j]] < C[i])
	 *       	C[i] = C[i-V[j]] + 1
	 *    
	 *    return C[S]
	 *    
	 *     Consider the following inputs
	 *     Values: 1,2,5,10.
	 *     Sum: 3
	 *     Output: C[]
	 *    
	 *     i = 0
	 *      C[0] = 0
	 *      coinCount.put(0,0)
	 *     i = 1
	 *     	 and j = 0
	 *     		   => V[j] = 1
	 *             i - V[j] = 0, and C[0] = 0, the number of coins needed for i = 1, is C[0] + 1, i.e. 1. C[1] = 1
	 *             coinCount.put(1,1)
	 *       and j = 1
	 *       	   => V[j] = 2
	 *             Since V[j] > i, for this and values above this the for loop is skipped
	 *     
	 *      i = 2
	 *      	and j = 0
	 *              => V[j] = 1
	 *      	    i - V[j] = 1, C[1] = 1, thus C[2] is updated to C[1]+1 i.e. 2. Thus C[2] = 2
	 *          and j = 1
	 *          	=> V[j] = 2
	 *          	i - V[j] = 0, C[0] = 0 and C[0] < C[2], thus C[2] is updated to C[0] + 1, i.e. 1. Thus C[2] = 1
	 *          for higher values, for loop is skipped
	 *          
	 *      i = 3 	
	 *    	    and j = 0
	 *              => V[j] = 1
	 *      	    i - V[j] = 2, C[2] = 1, thus C[3] is updated to C[2]+1 i.e. 2. Thus C[3] = 2
	 *          and j = 1
	 *           	=> V[j] = 2
	 *              i - V[j] = 1, C[1] = 1 < C[3] = 2, thus C[3] is updated to C[1] + 1 i.e. 2. C[3] = 2
	 *          
	 *      The output returned will be C[3]  
	 *  
	 *       
	 * */

	public int countChange(int []denoms, int sum) throws InvalidParameterException{
		// Impose input constraints
		if(denoms == null || sum < 0 || sum >= Integer.MAX_VALUE)
			throw new InvalidParameterException("No denominations are provided or the sum " +
					"provided is negative");

		int []count = new int[sum + 1];  // create and then the array to contain MAX_VALUE except for c[0]
		count[0] = 0;
		for(int i =1  ; i < count.length ; i++){
			count[i] = Integer.MAX_VALUE;
		}
		
		// Calculating sub-problems in a botton-up approach and storing it in c[]
		for(int i = 0; i < count.length ; i++){
			for(int j = 0; j < denoms.length; j++){
				if(denoms[j] <= i && count[i-denoms[j]] < count[i])
					count[i] = count[i-denoms[j]] + 1;
			}
		}
		// If the sum could be calculated using the given denominations return the count, else return -1
		if(count[sum] < Integer.MAX_VALUE)
			return count[sum];
		else 
			return -1;
	}

}
