package com.dp.tests.cases;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;

import org.junit.Test;

import com.dp.CoinChange;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 *
 */
public class NegativeSumTest {

		@Test (expected = InvalidParameterException.class)
		public void testCountChange() {
			CoinChange cc = new CoinChange();
			int []v = {1,2,5};
			int sum = -1;
			cc.countChange(v,sum);
		}

}
