package com.dp.tests.cases;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;

import org.junit.Test;

import com.dp.CoinChange;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 *
 */
public class CoinChangeNegativeTest {

	/**
	 * Test method for {@link com.dp.CoinChange#countChange(int[], int)}.
	 */
	@Test 
	public void testCountChange() {
		CoinChange cc = new CoinChange();
		int []v = {2,3,5};
		int sum = 1;
		int count = cc.countChange(v,sum);
		assertEquals(-1, count);
	}

}
