package com.dp.tests.cases;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;

import org.junit.Test;

import com.dp.CoinChange;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 *
 */
public class InvalidDenominationsTest {

	/**
	 * Test method for {@link com.dp.CoinChange#countChange(int[], int)}.
	 */
	@Test (expected = InvalidParameterException.class)
	public void testCountChange() {
		CoinChange cc = new CoinChange();
		int []v = null ;
		int sum = 10;
		cc.countChange(v,sum);
	}

}
