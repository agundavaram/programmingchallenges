package com.dp.tests.cases;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;

import org.junit.Test;

import com.dp.CoinChange;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 *
 */
public class CoinChangePositiveTest {

	/**
	 * Test method for {@link com.dp.CoinChange#countChange(int[], int)}.
	 */
	@Test 
	public void testCountChange() {
		CoinChange cc = new CoinChange();
		int []v = {1,2,5};
		int sum = 10;
		int count = cc.countChange(v,sum);
		assertEquals(2, count);
		sum = 8;
		count = cc.countChange(v,sum);
		assertEquals(3, count);
		
	}

}
