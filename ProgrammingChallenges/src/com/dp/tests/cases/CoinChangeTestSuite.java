package com.dp.tests.cases;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Amogha Gundavaram [amogha.gundavaram@gmail.com]
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ CoinChangePositiveTest.class, CoinChangeNegativeTest.class ,InvalidDenominationsTest.class,
		NegativeSumTest.class })
public class CoinChangeTestSuite {

}
